from django.shortcuts import render,HttpResponse,redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login , logout
from pprint import pprint
# Create your views here.

def login_view(request,**kwargs):
    if request.user.is_authenticated:
        return redirect('/bo-admin/')
    else:
        return render(request,'admin/auth/index.html')


def login_process(request):
    # return HttpResponse(request.POST.get('username'))
    if request.method == 'POST':
        user = authenticate(username=request.POST.get('username'), password=request.POST.get('password'))
        if user is not None:
           login(request, user)
           return redirect('/bo-admin/')
        else:
           return redirect('/bo-admin/login/')


@login_required(login_url='login')
def tes(request):
    return render(request,'admin/index.html')

def logouts(request):
    logout(request)
    return redirect('/bo-admin/login')

