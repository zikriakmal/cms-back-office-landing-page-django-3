from django.urls import path ,include
from admin import views
urlpatterns = [
    path('',views.tes,name='tes'),
    path('login/',views.login_view,name='login'),
    path('login/process',views.login_process),
    path('logout',views.logouts)
]
