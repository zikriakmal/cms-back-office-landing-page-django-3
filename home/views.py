from django.shortcuts import render,HttpResponse
from .models import Subscribe
from django.shortcuts import redirect

from django.contrib import messages
# Create your views here.

def home(request):
    context = {'nbar':'home'}
    return render(request, 'landing-page/index.html',context)

def insertSubs(request):
    if request.method=="POST":
        if request.POST.get('email'):
            subscribe = Subscribe()
            subscribe.email = request.POST.get('email')
            subscribe.save()
            messages.success(request, 'Changes successfully saved.')
            return redirect('home')
        else:
            return render(request, 'landing-page/index.html')